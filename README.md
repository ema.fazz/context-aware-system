# NoiSense - Crowdsensing platform for monitoring the noise in the world

This project is divided in two main platform:
 - a website where it is possible to see the measurements done by the users on a map;
 - an Android application with which users take noise measuraments and send them to the server to store them into the database.

